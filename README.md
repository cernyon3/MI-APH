PixiJS boilerplate for MI-APH
=========

## How to start

* Clone content of this repository into your local folder
* Install [Node Package Manager](https://www.npmjs.com)
* Execute command `npm install` and `npm start`
* Open `localhost:1234/index.html` in your browser

For more information read `report.pdf` document.