import Vec2 = require("../../ts/utils/Vec2");
import { PIXICmp } from "../../ts/engine/PIXIObject";
import { GRID_TILE_SIZE } from "./constants";
import { Grid } from "./grid";

/**
 * Place on a grid.
 */
export class GridPlace {
    /**
     * Position on the grid, in grid coordinates.
     */
    position: Vec2;

    /**
     * Game object residing on the place.
     */
    object: PIXICmp.Container;

    /**
     * Creates the grid place.
     * @param x The first coordinate of the grid place, in grid coordinates.
     * @param y The second coordinate of the grid place, in grid coordinates.
     */
    constructor(x: number, y: number) {
        this.position = new Vec2(x, y);
        this.object = null;
    }

    /**
     * Collects the adjecent grid place positions.
     * @param grid The grid this piece resides on.
     * @returns An array of grid places.
     */
    getNeighbors(grid: Grid) {
        let result = [];
        let myIndex = grid.getGridIndexFromGridCoordinates(this.position.x, this.position.y);
        let downIndex = myIndex + 1;
        let upIndex = myIndex - 1;
        let rightIndex = myIndex + grid.screenHeight / GRID_TILE_SIZE;
        let leftIndex = myIndex - grid.screenHeight / GRID_TILE_SIZE;
        let indices = [downIndex, upIndex, rightIndex, leftIndex];
        for (let i = 0; i < indices.length; ++i) {
            if (indices[i] >= 0 && indices[i] < grid.places.length) {
                result.push(grid.places[indices[i]].position);
            }
        }
        return result;
    }
}