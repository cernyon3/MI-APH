import { PIXICmp } from "../../ts/engine/PIXIObject";
import PIXIObjectBuilder from "../../ts/engine/PIXIObjectBuilder";
import Scene from "../../ts/engine/Scene";
import { KeyInputComponent } from "./components/key_input_component";
import { MenuComponent } from "./components/menu_component";
import { ParticlesComponent } from "./components/particles_component";
import { SoundComponent } from "./components/sound_component";
import { ATTR_GRID, GRID_TILE_SIZE, TEXTURE_BACKGROUND } from "./constants";
import { Factory } from "./factory";
import { Grid } from "./grid";

/**
 * Game containing object.
 */
export class Bombshell {

    /**
     * Creates the game.
     * Instantiates factory.
     */
    constructor() {
        new Factory();
    }

    /**
     * Called upon starting/restarting the game.
     * Instantiates every game object and component needed to play the game.
     * @param scene Game scene for objects to be inserted into.
     */
    init(scene: Scene) {
        scene.clearScene();
        let background = new PIXICmp.Sprite("background", PIXI.Texture.fromImage(TEXTURE_BACKGROUND));
        scene.stage.getPixiObj().addChild(background);
        scene.addGlobalAttribute(ATTR_GRID, new Grid(scene.app.screen));
        Factory.instance.createGUI(scene.app.stage, scene.app.screen);
        this.createSimpleArena(scene, background);
        scene.addGlobalComponent(new KeyInputComponent());
        scene.addGlobalComponent(new ParticlesComponent());
        scene.addGlobalComponent(new SoundComponent());
        scene.addGlobalComponent(new MenuComponent(this));
    }

    /**
     * Creates default randomized arena.
     * Creates player.
     * Creates AI enemy.
     * @param scene Game scene containing grid attribute and screen dimensions.
     * @param container Game container for objects to be inserted into.
     */
    createSimpleArena(scene: Scene, container: PIXICmp.Container) {
        this.addUnbreakableBlocks(scene, container);
        this.addBreakableBlocks(scene, container);
        Factory.instance.createPlayer(1.5 * GRID_TILE_SIZE, 1.5 * GRID_TILE_SIZE, container);
        Factory.instance.createEnemy(scene.app.screen.width - 1.5 * GRID_TILE_SIZE, scene.app.screen.height - 1.5 * GRID_TILE_SIZE, container);
    }

    /**
     * Creates unbreakable blocks in the arena.
     * @param scene Game scene containing grid attribute and screen dimensions.
     * @param container Game container for objects to be inserted into.
     */
    addUnbreakableBlocks(scene: Scene, container: PIXICmp.Container) {
        // outside rows
        let screen = scene.app.screen;
        let grid = <Grid>scene.getGlobalAttribute(ATTR_GRID);
        for (let gridX = 0; gridX < screen.width / GRID_TILE_SIZE; ++gridX) {
            let yTop = grid.getWorldCoordinateFromGridCoordinate(0);
            let yBot = grid.getWorldCoordinateFromGridCoordinate(screen.height / GRID_TILE_SIZE - 1);
            let worldX = grid.getWorldCoordinateFromGridCoordinate(gridX);
            this.addUnbreakableBlockTo(worldX, yTop, container);
            this.addUnbreakableBlockTo(worldX, yBot, container);
        }
        // outside columns
        for (let gridY = 1; gridY < screen.height / GRID_TILE_SIZE - 1; ++gridY) {
            let worldY = grid.getWorldCoordinateFromGridCoordinate(gridY);
            let xLeft = grid.getWorldCoordinateFromGridCoordinate(0);
            let xRight = grid.getWorldCoordinateFromGridCoordinate(screen.width / GRID_TILE_SIZE - 1);
            this.addUnbreakableBlockTo(xLeft, worldY, container);
            this.addUnbreakableBlockTo(xRight, worldY, container);
        }
        // inside grid
        let insideWidth = screen.width / GRID_TILE_SIZE - 2;
        let insideHeight = screen.height / GRID_TILE_SIZE - 2;
        for (let gridX = 2; gridX < insideWidth; gridX += 2) {
            for (let gridY = 2; gridY < insideHeight; gridY += 2) {
                let worldX = grid.getWorldCoordinateFromGridCoordinate(gridX);
                let worldY = grid.getWorldCoordinateFromGridCoordinate(gridY);
                this.addUnbreakableBlockTo(worldX, worldY, container);
            }
        }
    }

    /**
     * Creates breakable blocks in the arena.
     * @param scene Game scene containing grid attribute and screen dimensions.
     * @param container Game container for the objects to be inserted into.
     */
    addBreakableBlocks(scene: Scene, container: PIXICmp.Container) {
        // inside columns
        let screen = scene.app.screen;
        let grid = <Grid>scene.getGlobalAttribute(ATTR_GRID);
        let emptyPercentage = 0.5;
        let rightSide = screen.width / GRID_TILE_SIZE;
        let bottomSide = screen.height / GRID_TILE_SIZE;
        for (let gridX = 3; gridX < rightSide - 2; gridX += 2) {
            for (let gridY = 1; gridY < (gridX == rightSide - 4 ? bottomSide - 2 : bottomSide - 1); ++gridY) {
                if (Math.random() > emptyPercentage) {
                    let worldX = grid.getWorldCoordinateFromGridCoordinate(gridX);
                    let worldY = grid.getWorldCoordinateFromGridCoordinate(gridY);
                    this.addBreakableBlockTo(worldX, worldY, container);
                }
            }
        }
        // inside rows rest
        for (let gridX = 2; gridX < rightSide - 2; gridX += 2) {
            for (let gridY = 3; gridY < bottomSide - 2; gridY += 2) {
                if (Math.random() > emptyPercentage) {
                    let worldX = grid.getWorldCoordinateFromGridCoordinate(gridX);
                    let worldY = grid.getWorldCoordinateFromGridCoordinate(gridY);
                    this.addBreakableBlockTo(worldX, worldY, container);
                }
            }
        }
        // inside left and right
        let xLeft = grid.getWorldCoordinateFromGridCoordinate(1);
        let xRight = grid.getWorldCoordinateFromGridCoordinate(rightSide - 2);
        for (let gridY = 3; gridY < bottomSide - 3; ++gridY) {
            let worldY = grid.getWorldCoordinateFromGridCoordinate(gridY);
            if (Math.random() > emptyPercentage)
                this.addBreakableBlockTo(xLeft, worldY, container);
            if (Math.random() > emptyPercentage)
                this.addBreakableBlockTo(xRight, worldY, container);
        }
        // inside top and bottom
        let yTop = grid.getWorldCoordinateFromGridCoordinate(1);
        let yBot = grid.getWorldCoordinateFromGridCoordinate(bottomSide - 2);
        for (let gridX = 4; gridX < rightSide - 4; gridX += 2) {
            let worldX = grid.getWorldCoordinateFromGridCoordinate(gridX);
            if (Math.random() > emptyPercentage)
                this.addBreakableBlockTo(worldX, yTop, container);
            if (Math.random() > emptyPercentage)
                this.addBreakableBlockTo(worldX, yBot, container);
        }
    }

    /**
     * Creates a breakable block and inserts it into the grid attribute.
     * @param x The first coordinate of the created object.
     * @param y The second coordinate of the created object.
     * @param container The game container for the objects to be inserted into.
     */
    addBreakableBlockTo(x: number, y: number, container: PIXICmp.Container) {
        let block = Factory.instance.createBreakableBlock(x, y, container);
        this.addObjectOnGrid(x, y, block);
    }

    /**
     * Creates a unbreakable block and inserts it into the grid attribute.
     * @param x The first coordinate of the created object.
     * @param y The second coordinate of the created object.
     * @param container The game container for the objects to be inserted into.
     */
    addUnbreakableBlockTo(x: number, y: number, container: PIXICmp.Container) {
        let block = Factory.instance.createUnbreakableBlock(x, y, container);
        this.addObjectOnGrid(x, y, block);
    }

    /**
     * Inserts it into the grid attribute.
     * @param x The first coordinate of the created object.
     * @param y The second coordinate of the created object.
     * @param object The object inserted into the grid.
     */
    addObjectOnGrid(x: number, y: number, object: PIXICmp.Container) {
        let grid = <Grid>object.getScene().getGlobalAttribute(ATTR_GRID);
        let gridIndex = grid.getGridIndexFromWorldCoordinates(x, y);
        grid.places[gridIndex].object = object;
    }
}