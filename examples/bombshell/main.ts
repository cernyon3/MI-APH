import { PixiRunner } from '../../ts/PixiRunner';
import { Bombshell } from './bombshell';
import { SOUND_EXPLOSION, TEXTURE_BACKGROUND, TEXTURE_BOMB, TEXTURE_BOMB_EXPLOSION_PARTICLE, TEXTURE_BREAKABLE_BLOCK, TEXTURE_PLAYER, TEXTURE_UNBREAKABLE_BLOCK, TEXTURE_ENEMY } from './constants';

/**
 * The main class of the game.
 */
class Main {
    engine: PixiRunner;

    /**
     * Creates, loads and runs the game.
     */
    constructor() {
        this.engine = new PixiRunner();
        this.engine.init(document.getElementById("gameCanvas") as HTMLCanvasElement, 1);

        PIXI.loader
            .reset()    // necessary for hot reload
            .add(TEXTURE_BOMB, 'bomb.png')
            .add(TEXTURE_BREAKABLE_BLOCK, 'breakable.png')
            .add(TEXTURE_UNBREAKABLE_BLOCK, 'unbreakable.png')
            .add(TEXTURE_BACKGROUND, 'background.jpg')
            .add(TEXTURE_PLAYER, 'player_spritesheet.png')
            .add(TEXTURE_BOMB_EXPLOSION_PARTICLE, 'explosion_particle.png')
            .add(TEXTURE_ENEMY, 'enemy_spritesheet.png')
            .add(SOUND_EXPLOSION, 'explosion.ogg')
            .load(() => this.onAssetsLoaded());
    }

    onAssetsLoaded() {
        new Bombshell().init(this.engine.scene);
    }
}

new Main();
