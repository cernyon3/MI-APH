import { Rectangle } from "pixi.js";
import { GRID_TILE_SIZE } from "./constants";
import { GridPlace } from "./grid_place";
import Vec2 = require("../../ts/utils/Vec2");

/**
 * Representation of the game model for special entities, that reside on a grid.
 */
export class Grid {
    /**
     * Places on the grid.
     */
    places: GridPlace[];

    /**
     * Height of the canvas.
     */
    screenHeight: number;

    /**
     * Creates the grid.
     * @param screen The dimensions of the canvas.
     */
    constructor(screen: Rectangle) {
        this.places = [];
        this.screenHeight = screen.height;
        let screenWidth = screen.width;
        for (let x = 0; x < screenWidth / GRID_TILE_SIZE; ++x) {
            for (let y = 0; y < this.screenHeight / GRID_TILE_SIZE; ++y) {
                let index = this.getGridIndexFromGridCoordinates(x, y);
                this.places[index] = new GridPlace(x, y);
            }
        }
    }
    
    /**
     * Calculates manhattan distance between to positions, which are in grid coordinates.
     * @param pos1 The first position, in grid coordinates.
     * @param pos2 The seconds position, in grid coordinates.
     */
    manhattanDistanceGridCoordinates(pos1: Vec2, pos2: Vec2) {
        let dPosition = new Vec2(Math.abs(pos1.x - pos2.x), Math.abs(pos1.y - pos2.y));
        return dPosition.x + dPosition.y;
    }

    /**
     * Calculates the grid coordinate from world coordinate.
     * @param x World coordinate.
     */
    getGridCoordinateFromWorldCoordinate(x: number) {
        return x / GRID_TILE_SIZE - 0.5;
    }
    
    /**
     * Calculates the world coordinate from grid coordinate.
     * @param x 
     */
    getWorldCoordinateFromGridCoordinate(x: number) {
        return (x + 0.5) * GRID_TILE_SIZE;
    }

    /**
     * Calculates position of grid coordinates from world coordinates.
     * @param x The first coordinate, in world coordinates.
     * @param y The seconds coordinate, in world coordinates.
     */
    getGridCoordinatesFromWorldCoordinates(x: number, y: number) {
        let gridX = this.getGridCoordinateFromWorldCoordinate(x);
        let gridY = this.getGridCoordinateFromWorldCoordinate(y);
        return new Vec2(gridX, gridY);
    }

    /**
     * Calculates position of world coordinates from grid coordinates.
     * @param x The first coordinate, in grid coordinates.
     * @param y The second coordinate, in grid coordinates.
     */
    getWorldCoordinatesFromGridCoordinates(x: number, y: number) {
        let worldX = this.getWorldCoordinateFromGridCoordinate(x);
        let worldY = this.getWorldCoordinateFromGridCoordinate(y);
        return new Vec2(worldX, worldY);
    }

    /**
     * Calculates the index of a grid place, that would reside on given world coordinates.
     * @param x The first coordinate, in world coordinates.
     * @param y The second coordinate, in world coordinates.
     */
    getGridIndexFromWorldCoordinates(x: number, y: number) {
        let gridX = this.getGridCoordinateFromWorldCoordinate(x);
        let gridY = this.getGridCoordinateFromWorldCoordinate(y);
        return this.getGridIndexFromGridCoordinates(gridX, gridY);
    }

    /**
     * Calculates the index of a grid place, that would reside on given grid coordinates.
     * @param x The first coordinate, in grid coordinates.
     * @param y The seconds coordinate, in grid coordinates.
     */
    getGridIndexFromGridCoordinates(x: number, y: number) {
        return x * this.screenHeight / GRID_TILE_SIZE + y;
    }
    
    /**
     * Gets nearest grid place to given grid coordinates.
     * @param x The first coordinate, in grid coordinates.
     * @param y The second coordinate, in grid coordinates.
     */
    getGridPlaceGridCoordinates(x: number, y: number) {
        let index = this.getGridIndexFromGridCoordinates(x, y);
        return this.places[index];
    }

    /**
     * Gets nearest grid position given world coordinates.
     * @param x The first coordinate, in world coordinates.
     * @param y The second coordinate, in world coordinates.
     */
    getClosestGridPosition(x: number, y: number) {
        let result = this.getGridCoordinatesFromWorldCoordinates(x, y);
        result.x = Math.round(result.x);
        result.y = Math.round(result.y);
        return result;
    }
}