/**
 * Attribute containing direction of movement.
 */
export class Movement {
    side: number;
}