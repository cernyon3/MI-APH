export const MSG_BOMB_EXPLODED = "MSG_BOMB_EXPLODED";
export const MSG_BOMB_PLACED = "MSG_BOMB_PLACED"; 
export const MSG_ANY = "MSG_ANY";  // special key for global subscribers, usually for debugging
export const MSG_KEY_PRESSED = "MSG_KEY_PRESSED"; 
export const MSG_KEY_RELEASED = "MSG_KEY_RELEASED"; 
export const MSG_PLAY_SOUND = "MSG_PLAY_SOUND"; 
export const MSG_ENTITY_DESTROYED = "MSG_ENTITY_DESTROYED"; 
export const MSG_CREATED_VALUABLE = "MSG_CREATED_VALUABLE";
export const MSG_DESTROYED_VALUABLE = "MSG_DESTROYED_VALUABLE";
export const MSG_PLAYER_DEATH = "MSG_PLAYER_DEATH";

export const TEXTURE_PLAYER = "player";
export const TEXTURE_BOMB = "bomb";
export const TEXTURE_ENEMY = "enemy";
export const TEXTURE_BREAKABLE_BLOCK = "breakable";
export const TEXTURE_UNBREAKABLE_BLOCK = "unbreakable";
export const TEXTURE_BACKGROUND = "background";
export const TEXTURE_BOMB_EXPLOSION_PARTICLE = "explosion_particle";

export const SOUND_EXPLOSION = "explosion.ogg";

export const ATTR_MOVEMENT = "ATTR_MOVEMENT";
export const ATTR_GRID = "ATTR_GRID";

export const GRID_TILE_SIZE = 48;
export const SCREEN_HEIGHT = 720;
export const SCREEN_WIDTH = 1104;
export const FADEOUT_TIME_DEATH = 500;

export const KEY_LEFT = 37;
export const KEY_RIGHT = 39;
export const KEY_UP = 38;
export const KEY_DOWN = 40;
export const KEY_SPACE = 32;
export const KEY_W = "W".charCodeAt(0);
export const KEY_S = "S".charCodeAt(0);
export const KEY_A = "A".charCodeAt(0);
export const KEY_D = "D".charCodeAt(0);
export const KEY_R = "R".charCodeAt(0);
export const KEY_SHIFT = 16;

export const DIRECTION_DOWN = 0;
export const DIRECTION_RIGHT = 1;
export const DIRECTION_UP = 2;
export const DIRECTION_LEFT = 3;
    
export const TAG_BREAKABLE_WALL = "breakable_wall";
export const TAG_UNBREAKABLE_WALL = "unbreakable_wall";
export const TAG_BOMB = "bomb";
export const TAG_PLAYER = "player";
export const TAG_ENEMY = "enemy";