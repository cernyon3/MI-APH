import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { MSG_ENTITY_DESTROYED as MSG_ENTITY_DESTROYED, MSG_CREATED_VALUABLE, MSG_DESTROYED_VALUABLE } from "../constants";

/**
 * Handles releasing score into gui.
 */
export class ValuableComponent extends Component {

    /**
     * The score value to get upon destruction of this game object.
     */
    value: number;

    /**
     * Creates the component.
     * @param value The score value to get upon destruction of this game object.
     */
    constructor(value: number) {
        super();
        this.value = value;
    }

    /**
     * Called once upon start.
     * Subscribes to appropriate messages.
     * Sends message, that a valuable was created.
     */
    onInit() {
        this.sendMessage(MSG_CREATED_VALUABLE);
        this.subscribe(MSG_ENTITY_DESTROYED);
    }

    /**
     * Processes subscribed fired messages.
     * If destroyed, sends message that a valuable was destroyed.
     * @param msg The processed message object.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_ENTITY_DESTROYED && msg.gameObject == this.owner) {
            this.sendMessage(MSG_DESTROYED_VALUABLE, this.value);
            this.finish();
        }
    }
}