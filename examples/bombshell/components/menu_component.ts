import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { Bombshell } from "../bombshell";
import { KEY_R, MSG_KEY_PRESSED, MSG_PLAYER_DEATH } from "../constants";

/**
 * Handles game restarts.
 */
export class MenuComponent extends Component {

    /**
     * The game object which is to be restarted.
     */
    game: Bombshell;

    /**
     * Creates this component.
     * @param game The game object which is to be restarted.
     */
    constructor(game: Bombshell) {
        super();
        this.game = game;
    }

    /**
     * Called once upon start.
     * Subscribes itself to messages.
     */
    onInit() {
        this.subscribe(MSG_KEY_PRESSED);
    }

    /**
     * Handles message events.
     * Restarts the game.
     * @param msg The processed message object.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_KEY_PRESSED) {
            if (msg.data == KEY_R) {
                this.game.init(this.owner.getScene());
            }
        }
    }
}