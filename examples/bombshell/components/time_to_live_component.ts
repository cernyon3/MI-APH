import Component from "../../../ts/engine/Component";
import { PIXICmp } from "../../../ts/engine/PIXIObject";

/**
 * Handles gradual fade-out and disposal of game object.
 */
export class TimeToLiveComponent extends Component {

    /**
     * Max duration of this component, in milliseconds.
     * This doesn't change during the life of this component.
     */
    timeToLive: number;

    /**
     * Current duration of this component, in milliseconds.
     */
    timeAlive = 0;

    /**
     * Inverted value of timeToLive.
     */
    invertedTimeToLive: number;

    /**
     * Creates the component.
     * @param timeToLive Max duration of this component, in milliseconds.
     */
    constructor(timeToLive: number) {
        super();
        this.timeToLive = timeToLive;
        this.invertedTimeToLive = 1 / this.timeToLive;
    }

    /**
     * Called once every frame.
     * Updates current duration of life of this component.
     * Updates alpha value of the owner game object's sprite.
     * Destroys the game object and component after timeToLive milliseconds.
     * @param delta Time since the last frame, in milliseconds.
     * @param absolute Time since the start of the game, in milliseconds.
     */
    onUpdate(delta: number, absolute: number) {
        this.timeAlive += delta;
        let sprite = <PIXICmp.Sprite>this.owner;
        if (sprite != null) {
            let t = 1 - this.timeAlive * this.invertedTimeToLive;
            let mt = 1 - t;
            sprite.alpha = 1 - mt * mt;
        }
        if (this.timeAlive > this.timeToLive) {
            this.owner.remove();
            this.finish();
        }
    }
}