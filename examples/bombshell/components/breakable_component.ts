import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { ATTR_GRID, MSG_BOMB_EXPLODED, MSG_ENTITY_DESTROYED } from "../constants";
import { Grid } from "../grid";
import { TimeToLiveComponent } from "./time_to_live_component";

/**
 * Handles behavior of objects, that can be broken by bombs.
 */
export class BreakableComponent extends Component {

    /**
     * Called once upon start.
     * Subscribes to appropriate messages.
     */
    onInit() {
        this.subscribe(MSG_BOMB_EXPLODED);
    }
    
    /**
     * Processes subscribed fired messages.
     * Calculates, whether this bomb should be detonated based on the distance 
     * between the bombs and the other bomb's destructive power.
     * @param msg The processed message object.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_BOMB_EXPLODED) {
            let grid = <Grid>this.owner.getScene().getGlobalAttribute(ATTR_GRID);
            let bombObject = msg.gameObject.getPixiObj();
            let ownerObject = this.owner.getPixiObj();
            let bombPower = msg.data;
            let myGridPosition = grid.getClosestGridPosition(ownerObject.position.x, ownerObject.position.y);
            let bombGridPosition = grid.getClosestGridPosition(bombObject.position.x, bombObject.position.y);
            if (grid.manhattanDistanceGridCoordinates(myGridPosition, bombGridPosition) <= bombPower) {
                this.unsubscribe(MSG_BOMB_EXPLODED);
                let grid = <Grid>this.owner.getScene().getGlobalAttribute(ATTR_GRID);
                let index = grid.getGridIndexFromWorldCoordinates(ownerObject.position.x, ownerObject.position.y);
                grid.places[index].object = null;
                this.sendMessage(MSG_ENTITY_DESTROYED);
                this.owner.addComponent(new TimeToLiveComponent(500));
                this.finish();
            }
        }
    }
}