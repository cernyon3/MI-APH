import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { MSG_CREATED_VALUABLE, MSG_DESTROYED_VALUABLE, MSG_PLAYER_DEATH } from "../constants";

/**
 * Handles displaying miscellaneous text on the screen.
 */
export class GUIComponent extends Component {

    /**
     * Number of objects that prevent the player from winning.
     */
    valueableCount = 0;

    /**
     * Text object to display.
     */
    textObject: PIXICmp.Text;

    /**
     * Called once upon start.
     * Subscribes to appropriate messages and sets default values to unset member variables.
     */
    onInit() {
        this.subscribe(MSG_CREATED_VALUABLE);
        this.subscribe(MSG_DESTROYED_VALUABLE);
        this.subscribe(MSG_PLAYER_DEATH);
        this.textObject = <PIXICmp.Text> this.owner;
    }

    /**
     * Processes subscribed fired messages.
     * Writes winning or losing quote to the screen based on the count of valuables in the game.
     * @param msg The processed message object.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_CREATED_VALUABLE) {
            ++this.valueableCount;
        } else if (msg.action == MSG_DESTROYED_VALUABLE) {
            --this.valueableCount;
            if (this.valueableCount <= 0) {
                this.textObject.text = "You win!";
            }
        } else if (msg.action == MSG_PLAYER_DEATH) {
            this.textObject.text = "You lose!\nPress R to try again.";
        }
    }
}