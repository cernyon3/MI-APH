import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { MSG_DESTROYED_VALUABLE } from "../constants";

/**
 * Handles incrementing and displaying score on the screen.
 */
export class ScoreComponent extends Component {

    /**
     * The actual score value.
     */
    score = 0;

    /**
     * Called once upon start.
     * Subscribes to appropriate messages.
     * Displays initial score.
     */
    onInit() {
        this.subscribe(MSG_DESTROYED_VALUABLE);
        let text = <PIXICmp.Text>this.owner;
        text.text = `Score: ${this.score}`;
    }

    /**
     * Handles subscribed fired messages.
     * Updates score value and displays it.
     * @param msg The processed message.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_DESTROYED_VALUABLE) {
            this.score += msg.data;
            let text = <PIXICmp.Text>this.owner;
            text.text = `Score: ${this.score}`;
        }
    }
}