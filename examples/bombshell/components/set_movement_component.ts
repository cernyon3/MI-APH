import Component from "../../../ts/engine/Component";
import { Point } from "pixi.js";
import Vec2 = require("../../../ts/utils/Vec2");

/**
 * Handles random one way movement.
 */
export class SetMovementComponent extends Component {

    /**
     * Direction of movement.
     */
    direction: Vec2;

    /**
     * Called once upon start.
     * Randomizes direction of movement.
     */
    onInit() {
        let scaling = .5;
        let length = ((Math.random() * 0.5) + 0.5) * scaling;
        this.direction = new Vec2(Math.random(), Math.random()).normalize().multiply(length);
        if (Math.random() > 0.5) {
            this.direction.x = -this.direction.x;
        }
        if (Math.random() > 0.5) {
            this.direction.y = -this.direction.y;
        }
    }

    /**
     * Called once every frame.
     * Moves owner object in the randomized direction set on initialization.
     * @param delta Time since the last frame, in milliseconds.
     * @param absolute Time since the start of the game, in milliseconds.
     */
    onUpdate(delta: number, absolute: number) {
        let position = this.owner.getPixiObj().position;
        position.x += this.direction.x * delta;
        position.y += this.direction.y * delta;
    }
}