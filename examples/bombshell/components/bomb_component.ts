import { Point } from "pixi.js";
import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { ATTR_GRID, MSG_BOMB_EXPLODED, MSG_BOMB_PLACED } from "../constants";
import { Grid } from "../grid";
import { PIXICmp } from "../../../ts/engine/PIXIObject";

/**
 * Handles bomb behavior.
 */
export class BombComponent extends Component {

    /**
     * Distance in grid coordinates, where destructive effects are in play.
     */
    power = 1;

    /**
     * Time until explosion in milliseconds. 
     * This is constant - it remains the same during the life of the component.
     */
    origAliveTime = 1000;
    
    /**
     * Scale of the owner gameobject upon initialization.
     */
    origScale: Point;
    
    /**
     * Time until explosion in milliseconds. 
     * This changes during the life of the component.
     */
    aliveTime = this.origAliveTime;

    /**
     * Called once upon start.
     * Subscribes to appropriate messages and sets default values to unset member variables.
     */
    onInit() {
        this.subscribe(MSG_BOMB_EXPLODED);
        this.sendMessage(MSG_BOMB_PLACED);
        this.origScale = this.owner.getPixiObj().scale.clone();
    }

    /**
     * Processes subscribed fired messages.
     * @param msg The processed message object.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_BOMB_EXPLODED) {
            this.bombExploded(msg.data, msg.gameObject.getPixiObj());
        }
    }

    /**
     * Called once every frame.
     * Updates bomb's time alive and detonates it, when it reaches zero.
     * @param delta Time since the last frame in milliseconds.
     * @param absolute Time since the start of the game. 
     */
    onUpdate(delta: number, absolute: number) {
        this.aliveTime -= delta;
        let minScale = this.origScale.clone();
        minScale.x *= 0.5;
        minScale.y *= 0.5;
        let t = Math.abs(((this.aliveTime - this.origAliveTime * 0.5) / this.origAliveTime) * 2);
        t = t * t;
        let mt = 1 - t;
        let currentScale = new Point(this.origScale.x * t + minScale.x * mt, this.origScale.y * t + minScale.y * mt);
        this.owner.getPixiObj().scale = currentScale;
        if (this.aliveTime <= 0) {
            this.unsubscribe(MSG_BOMB_EXPLODED);
            this.sendMessage(MSG_BOMB_EXPLODED, this.power);
            this.owner.remove();
            this.finish();
        }
    }

    /**
     * Calculates, whether this bomb should be detonated based on the distance 
     * between the bombs and the other bomb's destructive power.
     * @param bombPower Destructive reach of the other bomb, in grid coordinates.
     * @param bombObject The other bomb.
     */
    bombExploded(bombPower: number, bombObject: PIXI.Container) {
        let bombPosition = bombObject.position;
        let myPosition = this.owner.getPixiObj().position;
        let grid = <Grid>this.owner.getScene().getGlobalAttribute(ATTR_GRID);
        let bombGridPosition = grid.getClosestGridPosition(bombPosition.x, bombPosition.y);
        let myGridPosition = grid.getClosestGridPosition(myPosition.x, myPosition.y);
        if (grid.manhattanDistanceGridCoordinates(myGridPosition, bombGridPosition) <= bombPower) {
            this.unsubscribe(MSG_BOMB_EXPLODED);
            this.sendMessage(MSG_BOMB_EXPLODED, this.power);
            this.owner.remove();
            this.finish();
        }
    }
}    