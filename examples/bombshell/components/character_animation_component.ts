import { BaseTexture, Rectangle, Sprite, Texture } from "pixi.js";
import Component from "../../../ts/engine/Component";
import { ATTR_MOVEMENT } from "../constants";
import { Movement } from "../movement";

/**
 * Handles animation of the game object's sprite.
 * Works with spritesheet that is 4 rows and 3 columns in size.
 * Each row represents one direction of the sprite.
 * Each column represents one animation frame in given direction of the sprite's tile.
 */
export class CharacterAnimationComponent extends Component {

    /**
     * The sprite to animate.
     */
    sprite: Sprite;

    /**
     * Separate textures for animation.
     */
    textures: Texture[][];

    /**
     * Current duration of this frame, in milliseconds.
     */
    secondsAnimatingCurrentTexture = 0;

    /**
     * Index of animation frame for given direction.
     * Must always be in interval [0, 3] inclusive.
     */
    currentIndex = 0;

    /**
     * Duration of one frame, in milliseconds.
     */
    oneFramePeriod = 100;

    /** 
     * The sprite sheet used as base texture.
     */
    baseTexture: BaseTexture;

    /**
     * Creates the component.
     * @param texture The sprite sheet used as base texture.
     */
    constructor(texture: BaseTexture) {
        super();
        this.baseTexture = texture;
    }

    /**
     * Called once upon start.
     * Sets textures for fast swapping during the animation.
     */
    onInit() {
        this.sprite = <Sprite>this.owner.getPixiObj();
        this.sprite.scale.set(1.2, 1.2);
        this.sprite.anchor.set(0.5, 0.5);
        this.textures = [];
        let rect = new Rectangle(0, 0, 99 / 3, 144 / 4);
        for (let j = 0; j < 3; ++j) {
            this.textures[j] = [];
            for (let i = 0; i < 4; ++i) {
                rect.x = j * rect.width;
                rect.y = i * rect.height;
                this.textures[j][i] = new Texture(this.baseTexture, rect);
            }
        }
        this.sprite.texture = this.textures[0][this.currentIndex];
    }

    /**
     * Called once every frame.
     * Calculates whether the sprite should be changed, and changes the sprite accordingly.
     * @param delta Time since the last frame, in milliseconds.
     * @param absolute Time since the start of the game, in milliseconds.
     */
    onUpdate(delta: number, absolute: number) {
        let movement = this.owner.getAttribute<Movement>(ATTR_MOVEMENT);
        if (movement.side >= 0) {
            this.secondsAnimatingCurrentTexture += delta;
            if (this.secondsAnimatingCurrentTexture > this.oneFramePeriod) {
                ++this.currentIndex;
                this.secondsAnimatingCurrentTexture = 0;
                if (this.currentIndex > 2) {
                    this.currentIndex = 0;
                }
            }
            this.sprite.texture = this.textures[this.currentIndex][movement.side];
        }
    }
}