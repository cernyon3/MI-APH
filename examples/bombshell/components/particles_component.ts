import { PI_2, Point, RAD_TO_DEG } from "pixi.js";
import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { MSG_BOMB_EXPLODED, TEXTURE_BOMB_EXPLOSION_PARTICLE } from "../constants";
import { SetMovementComponent } from "./set_movement_component";
import { TimeToLiveComponent } from "./time_to_live_component";

/**
 * Handles spawning particles.
 */
export class ParticlesComponent extends Component {

    /**
     * Called once upon start.
     * Subscribes component to appropriate messages.
     */
    onInit() {
        this.subscribe(MSG_BOMB_EXPLODED);
    }

    /**
     * Handles fired message events.
     * Creates particles on bomb explosions.
     * @param msg The processed message object.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_BOMB_EXPLODED) {
            let particleCount = 300;
            let particles = new PIXICmp.ParticleContainer("particles");
            particles.setProperties({
                scale: true,
                position: true,
                rotation: true,
                uvs: true,
                alpha: true
            });
            let bombObject = msg.gameObject.getPixiObj();
            let position = new Point(bombObject.position.x, bombObject.position.y);
            let maxTimeToLive = 200;
            bombObject.parent.addChildAt(particles, 1);
            for(let i = 0; i < particleCount; ++i) {
                let particle = new PIXICmp.Sprite("particle",PIXI.Texture.fromImage(TEXTURE_BOMB_EXPLOSION_PARTICLE));
                let timeToLive = Math.random() * maxTimeToLive;
                particle.position.set(position.x, position.y);
                particle.anchor.set(0.5, 0.5);
                particle.rotation = Math.random() * PI_2 * RAD_TO_DEG;
                particle.addComponent(new SetMovementComponent());
                particle.addComponent(new TimeToLiveComponent(timeToLive));
                particles.addChild(particle);
            }
            particles.addComponent(new TimeToLiveComponent(maxTimeToLive));
        }
    }
}