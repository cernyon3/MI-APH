import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { ATTR_GRID, ATTR_MOVEMENT, DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_UP, FADEOUT_TIME_DEATH, MSG_BOMB_EXPLODED, MSG_BOMB_PLACED, MSG_ENTITY_DESTROYED, TAG_BOMB, TAG_PLAYER } from "../constants";
import { Factory } from "../factory";
import { Grid } from "../grid";
import { GridPlace } from "../grid_place";
import { Movement } from "../movement";
import { TimeToLiveComponent } from "./time_to_live_component";
import Vec2 = require("../../../ts/utils/Vec2");

/**
 * Decides whether target should be followed or avoided.
 */
enum TargetType {
    Seek,
    Flee
}

/**
 * Handles AI behavior. 
 * It finds path to player, if it isn't under any threat, and flees from bombs, when they are placed.
 */
export class AIComponent extends Component {

    /**
     * Current target to seek/avoid.
     */
    target: PIXI.Container;

    /**
     * Defines whether the member variable target should be seeked or avoided.
     */
    targetType: TargetType;

    /**
     * Current path to seek.
     */
    path: GridPlace[];

    /**
     * Counts down to zero.
     * When this variable reaches zero, this component deploys a bomb on its position on grid.
     */
    bombLayingCountdown: number;

    /**
     * The movement speed of the object.
     */
    movementSpeed = 0.1;

    /**
     * How long the component waits when it has reached its target before deploying a bomb, in milliseconds.
     */
    countdownLength = 200;

    /**
     * Called once on component start.
     * Subscribes this component to appropriate messages and sets default values to unset member variables.
     */
    onInit() {
        this.subscribe(MSG_BOMB_PLACED);
        this.subscribe(MSG_BOMB_EXPLODED)
        this.path = <GridPlace[]>[];
        this.target = this.scene.findFirstObjectByTag(TAG_PLAYER).getPixiObj();
        this.targetType = TargetType.Seek;
    }

    /**
     * Processes subscribed fired messages.
     * @param msg The processed message object.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_BOMB_PLACED) {
            this.target = this.scene.findFirstObjectByTag(TAG_BOMB).getPixiObj();
            this.targetType = TargetType.Flee;
        } else if (msg.action == MSG_BOMB_EXPLODED) {
            let bombPosition = msg.gameObject.getPixiObj().position;
            let myPosition = this.getGroundPoint();
            let grid = <Grid>this.scene.getGlobalAttribute(ATTR_GRID);
            let bombGridPosition = grid.getGridCoordinatesFromWorldCoordinates(bombPosition.x, bombPosition.y);
            let myGridPosition = grid.getGridCoordinatesFromWorldCoordinates(myPosition.x, myPosition.y);
            if (grid.manhattanDistanceGridCoordinates(myGridPosition, bombGridPosition) < msg.data) {
                this.owner.addComponent(new TimeToLiveComponent(FADEOUT_TIME_DEATH));
                this.sendMessage(MSG_ENTITY_DESTROYED);
                this.finish();
            } else {
                let bombs = this.scene.findAllObjectsByTag(TAG_BOMB);
                if (bombs.length <= 1) {
                    let player = this.scene.findFirstObjectByTag(TAG_PLAYER);
                    // player might have died first
                    if (player != null) {
                        this.target = this.scene.findFirstObjectByTag(TAG_PLAYER).getPixiObj();
                        this.targetType = TargetType.Seek;
                    }
                } else {
                    this.target = bombs[0].getPixiObj();
                    this.targetType = TargetType.Flee;
                }
            }
        }
    }

    /**
     * Called once every frame, handles main component logic.
     * @param delta Time in milliseconds since the last frame.
     * @param absolute Time in milliseconds since the start of the game.
     */
    onUpdate(delta: number, absolute: number) {
        this.calculatePath();
        if (this.targetType == TargetType.Seek) {
            if (this.path.length > 0) {
                this.seekTarget(delta);
            } else {
                this.tryPlaceBomb(delta);
            }
        } else {
            this.fleeFromTarget(delta);
        }
    }

    /**
     * Transforms coordinates from world coordinates to grid coordinates and retrieves corresponding grid place.
     * @param x The first coordinate.
     * @param y The seconds coordinate.
     * @param grid Grid containing grid places.
     */
    getPlaceFromWorldCoordinates(x: number, y: number, grid: Grid) {
        let gridCoordinates = grid.getClosestGridPosition(x, y);
        let index = grid.getGridIndexFromGridCoordinates(gridCoordinates.x, gridCoordinates.y);
        let place = grid.places[index].position;
        return place;
    }

    /**
     * Calculates position where the feet of the sprite touch the ground.
     * @returns The calculated position.
     */
    getGroundPoint() {
        let point = this.owner.getPixiObj().position.clone();
        let yCorrection = 18;
        point.y += yCorrection;
        return point;
    }

    /**
     * Does a BFS search on global game grid and sets path if it is found.
     */
    calculatePath() {
        let grid = <Grid>this.owner.getScene().getGlobalAttribute(ATTR_GRID);
        let targetX = this.target.position.x;
        let targetY = this.target.position.y;
        let groundPosition = this.getGroundPoint();
        let myPlace = this.getPlaceFromWorldCoordinates(groundPosition.x, groundPosition.y, grid);
        let targetPlace = this.getPlaceFromWorldCoordinates(targetX, targetY, grid);
        let open = <Vec2[]>[];
        let closed = new Set<Vec2>();
        let backtrack = new Map<Vec2, Vec2>();
        let found = false;
        open.push(targetPlace);
        while (open.length > 0) {
            let openNode = open.shift();
            let openGridPlace = grid.getGridPlaceGridCoordinates(openNode.x, openNode.y);
            if (openNode == myPlace) {
                found = true;
                break;
            }
            if (!closed.has(openNode) && openGridPlace.object == null) {
                closed.add(openNode);
                let neighbors = openGridPlace.getNeighbors(grid);
                for (let i = 0; i < neighbors.length; ++i) {
                    if (!closed.has(neighbors[i])) {
                        backtrack.set(neighbors[i], openNode);
                        open.push(neighbors[i]);
                    }
                }
            }
        }
        if (found) {
            this.path = [];
            let pathTracingNode = backtrack.get(myPlace);
            if (pathTracingNode != null) {
                while (pathTracingNode != targetPlace) {
                    let gridPlaceIndex = grid.getGridIndexFromGridCoordinates(pathTracingNode.x, pathTracingNode.y);
                    let gridPlace = grid.places[gridPlaceIndex];
                    this.path.push(gridPlace);
                    pathTracingNode = backtrack.get(pathTracingNode);
                }
            }
        }
    }

    /**
     * Moves owner gameobject along the set path, if the path is set.
     * @param delta Time since last frame in milliseconds.
     */
    seekTarget(delta: number) {
        let movement = <Movement>this.owner.getAttribute(ATTR_MOVEMENT);
        let grid = <Grid>this.owner.getScene().getGlobalAttribute(ATTR_GRID);
        let acceptDistanceFromNodeSq = 25;
        let nextGridPosition = this.path[0].position;
        let nextPosition = grid.getWorldCoordinatesFromGridCoordinates(nextGridPosition.x, nextGridPosition.y);
        let myPosition = this.owner.getPixiObj().position;
        let position = this.getGroundPoint();
        let px2 = position.x - nextPosition.x;
        let py2 = position.y - nextPosition.y;
        px2 = px2 * px2;
        py2 = py2 * py2;
        let direction = new Vec2(nextPosition.x - position.x, nextPosition.y - position.y);
        if (direction.x != 0 || direction.y != 0) {
            direction = direction.normalize();
        }
        direction = direction.multiply(this.movementSpeed * delta);
        myPosition.x += direction.x;
        myPosition.y += direction.y;
        if (Math.abs(direction.x) > Math.abs(direction.y)) {
            movement.side = direction.x > 0 ? DIRECTION_RIGHT : DIRECTION_LEFT;
        } else {
            movement.side = direction.y > 0 ? DIRECTION_DOWN : DIRECTION_UP;
        }
        if (px2 + py2 <= acceptDistanceFromNodeSq) {
            this.path.shift();
        }
    }

    /**
     * Updates bomb countdown and places bomb, if the countdown is zero.
     * @param delta Time since last frame in milliseconds.
     */
    tryPlaceBomb(delta: number) {
        if (this.bombLayingCountdown > 0) {
            this.bombLayingCountdown -= delta;
            if (this.bombLayingCountdown <= 0) {
                let grid = <Grid>this.owner.getScene().getGlobalAttribute(ATTR_GRID);
                let position = this.getGroundPoint();
                let gridPosition = grid.getClosestGridPosition(position.x, position.y);
                let worldPosition = grid.getWorldCoordinatesFromGridCoordinates(gridPosition.x, gridPosition.y);
                Factory.instance.createBomb(worldPosition.x, worldPosition.y, this.owner.getPixiObj().parent);
            }
        } else {
            this.bombLayingCountdown = this.countdownLength;
        }
    }

    /**
     * Checks, if it is still within 3 grid spaces of the current threat.
     * If so, then it looks one tile from its own position on the grid and finds grid place to go to.
     * @param delta Time since last frame in milliseconds.
     */
    fleeFromTarget(delta: number) {
        let movement = <Movement>this.owner.getAttribute(ATTR_MOVEMENT);
        movement.side = -1;
        let grid = <Grid>this.scene.getGlobalAttribute(ATTR_GRID);
        let myPosition = this.getGroundPoint();
        let myGridPosition = grid.getClosestGridPosition(myPosition.x, myPosition.y);
        let targetGridPosition = grid.getClosestGridPosition(this.target.position.x, this.target.position.y);
        let currentTargetDistance = grid.manhattanDistanceGridCoordinates(myGridPosition, targetGridPosition);
        if (currentTargetDistance < 3) {
            let places = [];
            places.push(new Vec2(myGridPosition.x + 1, myGridPosition.y));
            places.push(new Vec2(myGridPosition.x - 1, myGridPosition.y));
            places.push(new Vec2(myGridPosition.x, myGridPosition.y + 1));
            places.push(new Vec2(myGridPosition.x, myGridPosition.y - 1));
            for (let i = 0; i < places.length; ++i) {
                let placeGridIndex = grid.getGridIndexFromGridCoordinates(places[i].x, places[i].y);
                if (grid.manhattanDistanceGridCoordinates(places[i], targetGridPosition) > currentTargetDistance
                    && grid.places[placeGridIndex].object == null) {
                    let worldPlaceI = grid.getWorldCoordinatesFromGridCoordinates(places[i].x, places[i].y);
                    let direction = new Vec2(worldPlaceI.x - myPosition.x, worldPlaceI.y - myPosition.y);
                    if (direction.x != 0 || direction.y != 0) {
                        direction = direction.normalize();
                    }
                    direction = direction.multiply(this.movementSpeed * delta);
                    let movingPosition = this.owner.getPixiObj().position;
                    movingPosition.x += direction.x;
                    movingPosition.y += direction.y;
                    if (Math.abs(direction.x) > Math.abs(direction.y)) {
                        movement.side = direction.x > 0 ? DIRECTION_RIGHT : DIRECTION_LEFT;
                    } else {
                        movement.side = direction.y > 0 ? DIRECTION_DOWN : DIRECTION_UP;
                    }
                    break;
                }
            }
        }
    }
}