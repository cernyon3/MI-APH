import Component from "../../../ts/engine/Component";
import { MSG_KEY_PRESSED, MSG_KEY_RELEASED } from "../constants";

/**
 * Handles key inputs.
 */
export class KeyInputComponent extends Component {

    /**
     * Contains pressed key codes.
     */
    keys = new Set<number>();

    /**
     * Called once upon start.
     * Attaches keyboard listeners.
     */
    onInit() {
        document.addEventListener("keydown", this.onKeyDown.bind(this));
        document.addEventListener("keyup", this.onKeyUp.bind(this));
    }

    /**
     * Handles key presses.
     * @param event Processed keyboard event.
     */
    onKeyDown(event: KeyboardEvent) {
        if (!this.keys.has(event.keyCode)) {
            this.keys.add(event.keyCode);
            this.sendMessage(MSG_KEY_PRESSED, event.keyCode);
        }
    }

    /**
     * Handles key releases.
     * @param event Processed keyboard event.
     */
    onKeyUp(event: KeyboardEvent) {
        this.keys.delete(event.keyCode);
        this.sendMessage(MSG_KEY_RELEASED, event.keyCode);
    }
}