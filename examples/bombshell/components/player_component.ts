import { Point } from "pixi.js";
import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { PIXICmp } from "../../../ts/engine/PIXIObject";
import { ATTR_GRID, ATTR_MOVEMENT, DIRECTION_DOWN, DIRECTION_LEFT, DIRECTION_RIGHT, DIRECTION_UP, FADEOUT_TIME_DEATH, GRID_TILE_SIZE, KEY_A, KEY_D, KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_S, KEY_SHIFT, KEY_UP, KEY_W, MSG_BOMB_EXPLODED, MSG_KEY_PRESSED, MSG_KEY_RELEASED, MSG_PLAYER_DEATH } from "../constants";
import { Factory } from "../factory";
import { Grid } from "../grid";
import { Movement } from "../movement";
import { TimeToLiveComponent } from "./time_to_live_component";
import Vec2 = require("../../../ts/utils/Vec2");

/**
 * Handles the player behavior.
 * Moves the object by
 */
export class PlayerComponent extends Component {

    isMovingLeft: boolean;
    isMovingRight: boolean;
    isMovingUp: boolean;
    isMovingDown: boolean;
    isPuttingBombDown: boolean;

    /**
     * Time since last bomb was dropped, in milliseconds.
     */
    lastBombDropTime = 0;

    /**
     * Time until next bomb can be dropped. 
     * This is constant - it remains the same during the life of this component.
     */
    bombDroppingPeriod = 200;

    /**
     * Called once upon start.
     * Subscribes to appropriate messages.
     */
    onInit() {
        this.subscribe(MSG_KEY_PRESSED);
        this.subscribe(MSG_KEY_RELEASED);
        this.subscribe(MSG_BOMB_EXPLODED);
    }

    /**
     * Processes messages.
     * @param msg The processed message object.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_KEY_PRESSED) {
            this.onPress(msg.data);
        } else if (msg.action == MSG_KEY_RELEASED) {
            this.onRelease(msg.data);
        } else if (msg.action == MSG_BOMB_EXPLODED) {
            this.bombExploded(msg.data, msg.gameObject);
        }
    }

    /**
     * Calculates, whether this components owner is caught in the blast radius of the bomb.
     * @param bombPower The distance of destructive effects of the bomb, in grid coordinates.
     * @param bomb The bomb game object.
     */
    bombExploded(bombPower: number, bomb: PIXICmp.ComponentObject) {
        let bombObject = bomb.getPixiObj();
        let bombPosition = bombObject.position;
        let myPosition = this.getGroundPoint();
        let grid = <Grid>this.owner.getScene().getGlobalAttribute(ATTR_GRID);
        let bombGridPosition = grid.getClosestGridPosition(bombPosition.x, bombPosition.y);
        let myGridPosition = grid.getClosestGridPosition(myPosition.x, myPosition.y);
        if (grid.manhattanDistanceGridCoordinates(myGridPosition, bombGridPosition) <= bombPower) {
            this.owner.addComponent(new TimeToLiveComponent(FADEOUT_TIME_DEATH));
            this.sendMessage(MSG_PLAYER_DEATH);
            this.finish();
        }
    }
    
    /**
     * Translate key presses into logical values of member variables.
     * @param keyCode The processed key code. 
     * See javascript keyboard keycodes for more info.
     */
    onPress(keyCode: number) {
        if (keyCode == KEY_LEFT || keyCode == KEY_A) {
            this.isMovingLeft = true;
        }
        if (keyCode == KEY_RIGHT || keyCode == KEY_D) {
            this.isMovingRight = true;
        }
        if (keyCode == KEY_UP || keyCode == KEY_W) {
            this.isMovingUp = true;
        }
        if (keyCode == KEY_DOWN || keyCode == KEY_S) {
            this.isMovingDown = true;
        }
        if (keyCode == KEY_SHIFT) {
            this.isPuttingBombDown = true;
        }
    }
    /**
     * Translate key presses into logical values of member variables.
     * @param keyCode The processed key code. 
     * See javascript keyboard keycodes for more info.
     */
    onRelease(keyCode: number) {
        if (keyCode == KEY_LEFT || keyCode == KEY_A) {
            this.isMovingLeft = false;
        }
        if (keyCode == KEY_RIGHT || keyCode == KEY_D) {
            this.isMovingRight = false;
        }
        if (keyCode == KEY_UP || keyCode == KEY_W) {
            this.isMovingUp = false;
        }
        if (keyCode == KEY_DOWN || keyCode == KEY_S) {
            this.isMovingDown = false;
        }
    }
    
    /**
     * Calculates position where the feet of the sprite touch the ground.
     * @returns The calculated position.
     */
    getGroundPoint() {
        let point = this.owner.getPixiObj().position.clone();
        let playerYCorrection = 18;
        point.y += playerYCorrection;
        return point;
    }

    /**
     * Called once every frame, handles main component logic.
     * Transforms logic member variables into movement and bomb dropping.
     * Handles collision detection.
     * @param delta Time in milliseconds since the last frame.
     * @param absolute Time in milliseconds since the start of the game.
     */
    onUpdate(delta: number, absolute: number) {
        let speed = 0.2 * delta;
        let movement = this.owner.getAttribute<Movement>(ATTR_MOVEMENT);
        let dPosition = new Point(0, 0);
        if (this.isMovingDown) {
            dPosition.y = speed;
            movement.side = DIRECTION_DOWN;
        } else if (this.isMovingUp) {
            dPosition.y = -speed;
            movement.side = DIRECTION_UP;
        }
        if (this.isMovingRight) {
            dPosition.x = speed;
            movement.side = DIRECTION_RIGHT;
        } else if (this.isMovingLeft) {
            dPosition.x = -speed;
            movement.side = DIRECTION_LEFT;
        }
        let grid = <Grid>this.owner.getScene().getGlobalAttribute(ATTR_GRID);
        let groundPoint = this.getGroundPoint();
        let myGridPosition = grid.getGridCoordinatesFromWorldCoordinates(groundPoint.x, groundPoint.y);
        let myRoundedGridPosition = new Vec2(Math.round(myGridPosition.x), Math.round(myGridPosition.y));
        let nextPos = new Vec2(groundPoint.x + dPosition.x, groundPoint.y + dPosition.y);
        let nextGridPosition = grid.getGridCoordinatesFromWorldCoordinates(nextPos.x, nextPos.y);
        let nextRoundedGridPosition = new Vec2(Math.round(nextGridPosition.x), Math.round(nextGridPosition.y));
        let nextGridPlaceIndex = grid.getGridIndexFromGridCoordinates(nextRoundedGridPosition.x, nextRoundedGridPosition.y);
        let nextGridPlace = grid.places[nextGridPlaceIndex];
        if (myGridPosition != nextGridPosition && nextGridPlace.object != null) {
            if (Math.round(nextGridPosition.x) != Math.round(myGridPosition.x)) {
                dPosition.x = 0;
            }
            if (Math.round(nextGridPosition.y) != Math.round(myGridPosition.y)) {
                dPosition.y = 0;
            }
        }
        let shallPass = true;
        if (myRoundedGridPosition.x != nextRoundedGridPosition.x
            && myRoundedGridPosition.y != nextRoundedGridPosition.y) {
            let checkIndex1 = grid.getGridIndexFromGridCoordinates(myRoundedGridPosition.x - nextRoundedGridPosition.x, nextGridPlaceIndex);
            let checkIndex2 = nextGridPlaceIndex + myRoundedGridPosition.y - nextRoundedGridPosition.y;
            shallPass = grid.places[checkIndex1].object == null;
            shallPass = shallPass || grid.places[checkIndex2].object == null;
        }
        if (!shallPass) {
            dPosition.x = 0;
            dPosition.y = 0;
        }
        this.owner.getPixiObj().position.x += dPosition.x;
        this.owner.getPixiObj().position.y += dPosition.y;
        if (dPosition.x == 0 && dPosition.y == 0) {
            movement.side = -1;
        }
        if (this.isPuttingBombDown) {
            this.tryCreateBomb(absolute);
            this.isPuttingBombDown = false;
        }
    }

    /**
     * Handles bomb dropping.
     * Creates a bomb if the last bomb was created before the last bomb dropping period.
     * @param time Time since the start of the game drop, in milliseconds.
     */
    tryCreateBomb(time: number) {
        if (time >= this.lastBombDropTime + this.bombDroppingPeriod) {
            this.lastBombDropTime = time;
            let bombPosition = this.getGroundPoint();
            bombPosition.x -= bombPosition.x % GRID_TILE_SIZE;
            bombPosition.y -= bombPosition.y % GRID_TILE_SIZE;
            bombPosition.x += GRID_TILE_SIZE * 0.5;
            bombPosition.y += GRID_TILE_SIZE * 0.5;
            Factory.instance.createBomb(bombPosition.x, bombPosition.y, this.owner.getPixiObj().parent);
        }
    }
}