import Component from "../../../ts/engine/Component";
import Msg from "../../../ts/engine/Msg";
import { MSG_BOMB_EXPLODED, SOUND_EXPLOSION } from "../constants";
import "../../../libs/pixi-sound/pixi-sound";

/**
 * Handles sound.
 */
export class SoundComponent extends Component {

    /**
     * Called once upon start.
     * Subscribes to appropriate messages.
     */
    onInit() {
        this.subscribe(MSG_BOMB_EXPLODED);
    }

    /**
     * Handles subscribed fired messages.
     * Plays sound upon bomb explosion.
     */
    onMessage(msg: Msg) {
        if (msg.action == MSG_BOMB_EXPLODED) {
            let sound = PIXI.sound.Sound.from(SOUND_EXPLOSION);
            sound.play();
        }
    }
}