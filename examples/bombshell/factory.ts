import { Container, Rectangle } from "pixi.js";
import { PIXICmp } from "../../ts/engine/PIXIObject";
import { AIComponent } from "./components/ai_component";
import { BombComponent } from "./components/bomb_component";
import { BreakableComponent } from "./components/breakable_component";
import { CharacterAnimationComponent } from "./components/character_animation_component";
import { GUIComponent } from "./components/gui_component";
import { PlayerComponent } from "./components/player_component";
import { ScoreComponent } from "./components/score_component";
import { ValuableComponent } from "./components/valuable_component";
import { ATTR_MOVEMENT, GRID_TILE_SIZE, TAG_BOMB, TAG_BREAKABLE_WALL, TAG_ENEMY, TAG_PLAYER, TAG_UNBREAKABLE_WALL, TEXTURE_BOMB, TEXTURE_BREAKABLE_BLOCK, TEXTURE_ENEMY, TEXTURE_PLAYER, TEXTURE_UNBREAKABLE_BLOCK } from "./constants";
import { Movement } from "./movement";

/**
 * Handles creation of game objects.
 */
export class Factory {

    static instance: Factory;

    /**
     * Creates the factory.
     */
    constructor() {
        Factory.instance = this;
    }

    /**
     * Creates gui.
     * @param container Game container for gui to be inserted into.
     * @param screen Dimensions of the canvas.
     */
    createGUI(container: PIXI.Container, screen: Rectangle) {
        let guiText = new PIXICmp.Text("gui");
        container.addChild(guiText);
        guiText.style.stroke = 0;
        guiText.style.strokeThickness = 5;
        guiText.style.fill = 0xFFFFFF;
        guiText.style.align = "center";
        guiText.position.set(screen.width * 0.5, screen.height * 0.5);
        guiText.anchor.set(0.5, 0.5);
        guiText.addComponent(new GUIComponent());
        let score = new PIXICmp.Text("score");
        guiText.addChild(score);
        score.style.stroke = 0;
        score.style.strokeThickness = 5;
        score.style.fill = 0xFFFFFF;
        score.position.set(10 - guiText.position.x, 10 - guiText.position.y);
        score.addComponent(new ScoreComponent());
    }

    /**
     * Creates a bomb.
     * @param x The first coordinate of the bomb.
     * @param y The second coordinate of the bomb.
     * @param container Game container for bomb to be inserted into.
     */
    createBomb(x: number, y: number, container: Container) {
        let bomb = new PIXICmp.Sprite(TAG_BOMB, PIXI.Texture.fromImage(TEXTURE_BOMB));
        bomb.anchor.set(0.5, 0.5);
        bomb.position.set(x, y);
        let scale = (1 / (bomb.width / GRID_TILE_SIZE));
        bomb.scale.set(scale, scale);
        container.addChildAt(bomb, 1);
        bomb.addComponent(new BombComponent());
    }

    /**
     * Creates a player.
     * @param x The first coordinate of the player.
     * @param y The seconds coordinate of the player.
     * @param container Game container for player to be inserted into.
     */
    createPlayer(x: number, y: number, container: PIXICmp.Container) {
        let player = new PIXICmp.Sprite(TAG_PLAYER, PIXI.Texture.fromImage(TEXTURE_PLAYER));
        let scale = 1 / (player.width / GRID_TILE_SIZE);
        player.scale.set(scale, scale);
        player.position.set(x, y);
        container.addChild(player);
        player.addAttribute(ATTR_MOVEMENT, new Movement());
        player.addComponent(new PlayerComponent());
        player.addComponent(new CharacterAnimationComponent(player.texture.baseTexture));
        
    }

    /**
     * Creates an enemy.
     * @param x The first coordinate of the enemy.
     * @param y The second coordinate of the enemy.
     * @param container Game container for enemy to be inserted into.
     */
    createEnemy(x: number, y: number, container: PIXICmp. Container) {
        let enemy = new PIXICmp.Sprite(TAG_ENEMY, PIXI.Texture.fromImage(TEXTURE_ENEMY));
        enemy.anchor.set(0.5, 0.5);
        let scale = 1 / (enemy.width / GRID_TILE_SIZE);
        enemy.scale.set(scale, scale);
        enemy.position.set(x, y);
        container.addChild(enemy);
        enemy.addAttribute(ATTR_MOVEMENT, new Movement());
        enemy.addComponent(new AIComponent());
        enemy.addComponent(new CharacterAnimationComponent(enemy.texture.baseTexture));
        enemy.addComponent(new ValuableComponent(10));
    }

    /**
     * Creates a breakable block.
     * @param x The first coordinate of the block.
     * @param y The second coordinate of the block.
     * @param container Game container for block to be inserted into.
     */
    createBreakableBlock(x: number, y: number, container: PIXICmp.Container) {
        let block = new PIXICmp.Sprite(TAG_BREAKABLE_WALL, PIXI.Texture.fromImage(TEXTURE_BREAKABLE_BLOCK));
        let scale = 1 / (block.width / GRID_TILE_SIZE);
        block.anchor.set(0.5, 0.5);
        block.scale.set(scale, scale);
        block.position.set(x, y);
        container.addChild(block);
        block.addComponent(new BreakableComponent());
        block.addComponent(new ValuableComponent(1));
        return block;
    }
    
    /**
     * Creates an unbreakable block.
     * @param x The first coordinate of the block.
     * @param y The second coordinate of the block.
     * @param container Game container for block to be inserted into.
     */
    createUnbreakableBlock(x: number, y: number, container: PIXICmp.Container) {
        let block = new PIXICmp.Sprite(TAG_UNBREAKABLE_WALL, PIXI.Texture.fromImage(TEXTURE_UNBREAKABLE_BLOCK));
        let scale = 1 / (block.width / GRID_TILE_SIZE);
        block.anchor.set(0.5, 0.5);
        block.scale.set(scale, scale);
        block.position.set(x, y);
        container.addChild(block);
        return block;
    }
}